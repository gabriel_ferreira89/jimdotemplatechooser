package br.com.gabrielferreira.jimdo.presentation.internal.di

import br.com.gabrielferreira.jimdo.presentation.internal.di.scope.ControllerScope
import br.com.gabrielferreira.jimdo.presentation.view.activity.MainActivity
import br.com.gabrielferreira.jimdo.presentation.view.activity.SplashActivity
import br.com.gabrielferreira.jimdo.presentation.view.presenter.MainPresenter
import br.com.gabrielferreira.jimdo.presentation.view.presenter.SplashPresenter
import dagger.Subcomponent

@ControllerScope
@Subcomponent(modules = [ControllerModule::class])
interface ControllerComponent {

    // Application
    fun inject(appApplication: AppApplication)

    // Presenter
    fun splashPresenter(): SplashPresenter

    fun mainPresenter(): MainPresenter

    // View
    fun inject(appApplication: SplashActivity)

    fun inject(mainActivity: MainActivity)
}