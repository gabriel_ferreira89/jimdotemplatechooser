package br.com.gabrielferreira.jimdo.presentation.util.extension

fun <K, V> Map<out K, V>.getOrDefaultCompat(key: K, defaultValue: V): V {
    if (this.containsKey(key)) {
        return this[key] ?: defaultValue
    }
    return defaultValue
}