package br.com.gabrielferreira.jimdo.presentation.internal.di

import android.content.Context
import android.content.SharedPreferences
import br.com.gabrielferreira.jimdo.BuildConfig
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val appApplication: AppApplication) {

    @Provides
    fun provideApplicationContext(): Context = appApplication

    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
            context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)
}