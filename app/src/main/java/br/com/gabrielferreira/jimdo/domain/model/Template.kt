package br.com.gabrielferreira.jimdo.domain.model

import android.support.annotation.ColorInt

class Template (val name: String,
                val active: Boolean,
                val screenshot: String,
                @ColorInt
                val backgroundColor: Int,
                val variationList: List<TemplateVariation>)