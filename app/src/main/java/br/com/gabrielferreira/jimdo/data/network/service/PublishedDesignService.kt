package br.com.gabrielferreira.jimdo.data.network.service

import br.com.gabrielferreira.jimdo.data.model.TemplateData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface PublishedDesignService {

    @GET("published_designs")
    fun getTemplateList(): Observable<List<String>>

    @GET
    fun getTemplate(@Url url: String): Observable<TemplateData>
}