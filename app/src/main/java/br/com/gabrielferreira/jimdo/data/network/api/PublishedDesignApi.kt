package br.com.gabrielferreira.jimdo.data.network.api

import br.com.gabrielferreira.jimdo.data.model.TemplateData
import br.com.gabrielferreira.jimdo.data.network.service.PublishedDesignService
import io.reactivex.Observable
import javax.inject.Inject

class PublishedDesignApi @Inject constructor() : BaseApi() {

    private var publishedService: PublishedDesignService

    init {
        val retrofit = build()
        publishedService = retrofit.create(PublishedDesignService::class.java)
    }

    fun getTemplateList(): Observable<List<String>> = publishedService.getTemplateList()
    fun getTemplate(url: String): Observable<TemplateData> = publishedService.getTemplate(url)
}