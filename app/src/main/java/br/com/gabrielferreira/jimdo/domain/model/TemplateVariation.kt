package br.com.gabrielferreira.jimdo.domain.model

import android.support.annotation.ColorInt

class TemplateVariation (val id: Int,
                         val name: String,
                         val iconType: IconType,
                         @ColorInt
                         val color: Int,
                         val screenshot: String)