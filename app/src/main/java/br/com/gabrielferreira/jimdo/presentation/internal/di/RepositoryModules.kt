package br.com.gabrielferreira.jimdo.presentation.internal.di

import br.com.gabrielferreira.jimdo.data.repository.TemplateDataRepository
import br.com.gabrielferreira.jimdo.domain.repository.TemplateRepository
import br.com.gabrielferreira.jimdo.presentation.internal.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class RepositoryModules{

    @Provides
    @ApplicationScope
    fun provideTemplateRepository(repository: TemplateDataRepository): TemplateRepository = repository
}