package br.com.gabrielferreira.jimdo.presentation.view.presenter

import br.com.gabrielferreira.jimdo.presentation.view.SplashContract
import java.util.*
import javax.inject.Inject

class SplashPresenter @Inject constructor() : BasePresenter<SplashContract.View>(), SplashContract.Presenter {

    companion object {
        private const val SPLASH_DELAY = 1000L
    }

    override fun onInitialize() {
        scheduleToMain()
    }

    private fun scheduleToMain() {
        val splashTimer = Timer()
        val splashTimerTask = object : TimerTask() {
            override fun run() {
                view?.redirectMain()
            }
        }
        splashTimer.schedule(splashTimerTask, SPLASH_DELAY)
    }
}
