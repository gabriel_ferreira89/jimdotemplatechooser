package br.com.gabrielferreira.jimdo.presentation.view.presenter

import android.util.Log
import br.com.gabrielferreira.jimdo.domain.model.Template
import br.com.gabrielferreira.jimdo.domain.usecase.TemplateListUseCase
import br.com.gabrielferreira.jimdo.presentation.util.extension.TAG
import br.com.gabrielferreira.jimdo.presentation.view.MainContract
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainPresenter @Inject constructor(private val templateListUseCase: TemplateListUseCase) :
        BasePresenter<MainContract.View>(), MainContract.Presenter {

    override fun init() {
        fetchLatestTemplates()
    }

    private fun fetchLatestTemplates() {
        templateListUseCase.fetchLatestTemplates(object : Observer<Template> {
            override fun onComplete() {
                Log.d(TAG, "onComplete")
            }

            override fun onNext(t: Template) {
                view?.addAdapter(t)
            }

            override fun onSubscribe(d: Disposable) {
                view?.setupViewPager()
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "onError")
            }
        })
    }
}