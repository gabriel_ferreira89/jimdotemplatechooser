package br.com.gabrielferreira.jimdo.domain.usecase

import br.com.gabrielferreira.jimdo.domain.model.Template
import br.com.gabrielferreira.jimdo.domain.repository.TemplateRepository
import io.reactivex.Observer
import javax.inject.Inject

class TemplateListUseCase @Inject constructor(private val templateRepository: TemplateRepository) : BaseUseCase() {

    fun fetchLatestTemplates(observer: Observer<Template>) {
        templateRepository.fetchLatestTemplates()
                .flatMap {
                    templateRepository.fetchTemplate(it)
                }
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler)
                .subscribe(observer)
    }
}