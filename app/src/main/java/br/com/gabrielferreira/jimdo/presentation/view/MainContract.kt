package br.com.gabrielferreira.jimdo.presentation.view

import br.com.gabrielferreira.jimdo.domain.model.Template

interface MainContract {

    interface View : BaseContract.View {
        fun setupViewPager()
        fun addAdapter(t: Template)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun init()
    }
}
