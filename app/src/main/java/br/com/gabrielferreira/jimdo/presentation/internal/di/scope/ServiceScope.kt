package br.com.gabrielferreira.jimdo.presentation.internal.di.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ServiceScope