package br.com.gabrielferreira.jimdo.data.mapper

import android.graphics.Color

open class BaseDataMapper {

    internal fun getColorFromString(color: String): Int =
            try {
                Color.parseColor(color)
            } catch (e: Exception) {
                Color.parseColor("#ffffff")
            }

    internal fun getScreenshot(screenshots: Map<String, String>) =
    //TODO do some logic to decide which image should we use
            if (screenshots.containsKey("medium")) {
                screenshots["iphone"] ?: ""
            } else {
                ""
            }
}