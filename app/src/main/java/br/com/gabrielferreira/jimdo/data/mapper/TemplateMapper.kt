package br.com.gabrielferreira.jimdo.data.mapper

import br.com.gabrielferreira.jimdo.data.model.TemplateData
import br.com.gabrielferreira.jimdo.domain.model.Template
import br.com.gabrielferreira.jimdo.presentation.util.extension.getOrDefaultCompat
import javax.inject.Inject

class TemplateMapper @Inject constructor(private val variationMapper: TemplateVariationMapper): BaseDataMapper() {

    fun map(data: TemplateData): Template {
        val backgroundColor = getColorFromString(data.meta.getOrDefaultCompat("color", ""))
        return Template(name = data.name ?: "",
                active = data.active ?: false,
                screenshot = getScreenshot(data.screenshots),
                backgroundColor = backgroundColor,
                variationList = data.variations.map { variationMapper.map(it) })
    }
}