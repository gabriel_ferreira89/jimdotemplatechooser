package br.com.gabrielferreira.jimdo.data.mapper

import br.com.gabrielferreira.jimdo.data.model.TemplateVariationData
import br.com.gabrielferreira.jimdo.domain.model.IconType
import br.com.gabrielferreira.jimdo.domain.model.TemplateVariation
import javax.inject.Inject

class TemplateVariationMapper @Inject constructor(): BaseDataMapper() {

    fun map(data: TemplateVariationData): TemplateVariation {

        val iconType = getIconType(data.iconType ?: "")
        var color = 0

        if (iconType == IconType.COLOR && data.icon != null
                && !data.icon.isNullOrEmpty()) {
            color = getColorFromString(data.icon)
        }

        return TemplateVariation(id = data.id ?: 0,
                name = data.name ?: "",
                iconType = iconType,
                color = color,
                screenshot = getScreenshot(data.screenshots))
    }

    private fun getIconType(type: String): IconType =
            when (type.toUpperCase()) {
                IconType.COLOR.toString() -> IconType.COLOR
                else -> IconType.NONE
            }
}