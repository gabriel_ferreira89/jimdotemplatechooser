package br.com.gabrielferreira.jimdo.data.repository

import br.com.gabrielferreira.jimdo.data.mapper.TemplateMapper
import br.com.gabrielferreira.jimdo.data.network.api.PublishedDesignApi
import br.com.gabrielferreira.jimdo.domain.model.Template
import br.com.gabrielferreira.jimdo.domain.repository.TemplateRepository
import io.reactivex.Observable
import javax.inject.Inject

class TemplateDataRepository @Inject constructor(private val publishedApi: PublishedDesignApi,
                                                 private val templateMapper: TemplateMapper) : TemplateRepository {

    override fun fetchLatestTemplates(): Observable<String> =
            publishedApi.getTemplateList().flatMapIterable { it }

    override fun fetchTemplate(url: String): Observable<Template> =
            publishedApi.getTemplate(url).map { templateMapper.map(it) }
}
