package br.com.gabrielferreira.jimdo.presentation.view.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import br.com.gabrielferreira.jimdo.R
import br.com.gabrielferreira.jimdo.domain.model.Template
import br.com.gabrielferreira.jimdo.presentation.util.extension.hide
import br.com.gabrielferreira.jimdo.presentation.util.extension.loadWithProgressPlaceholder
import br.com.gabrielferreira.jimdo.presentation.util.extension.show

class TemplateListAdapter(val context: Context,
                          var data: MutableList<Template> = mutableListOf()) : PagerAdapter() {

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val modelObject = data[position]
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.item_template, collection, false) as ViewGroup

        val background: ImageView = layout.findViewById(R.id.template_item_image)
        val name: TextView = layout.findViewById(R.id.template_item_name)
        val firstVariation: Button = layout.findViewById(R.id.template_item_variation_1)
        val secondVariation: Button = layout.findViewById(R.id.template_item_variation_2)
        val thirdVariation: Button = layout.findViewById(R.id.template_item_variation_3)
        val fourthVariation: Button = layout.findViewById(R.id.template_item_variation_4)
        val fifthVariation: Button = layout.findViewById(R.id.template_item_variation_5)

        background.loadWithProgressPlaceholder(modelObject.screenshot)

        name.text = modelObject.name

        if (modelObject.variationList.isNotEmpty()){
            firstVariation.backgroundTintList = ColorStateList.valueOf(modelObject.backgroundColor)
            firstVariation.setOnClickListener {
                background.loadWithProgressPlaceholder(modelObject.screenshot)
            }
            firstVariation.show()
        }else{
            firstVariation.hide()
        }

        if (modelObject.variationList.isNotEmpty()) {
            secondVariation.backgroundTintList = ColorStateList.valueOf(modelObject.variationList.first().color)
            secondVariation.setOnClickListener {
                background.loadWithProgressPlaceholder(modelObject.variationList.first().screenshot)
            }
            secondVariation.show()
        }else{
            secondVariation.hide()
        }

        if (modelObject.variationList.size >= 2) {
            thirdVariation.backgroundTintList = ColorStateList.valueOf(modelObject.variationList[1].color)
            thirdVariation.setOnClickListener {
                background.loadWithProgressPlaceholder(modelObject.variationList[1].screenshot)
            }
            thirdVariation.show()
        }else{
            thirdVariation.hide()
        }

        if (modelObject.variationList.size >= 3) {
            fourthVariation.backgroundTintList = ColorStateList.valueOf(modelObject.variationList[2].color)
            fourthVariation.setOnClickListener {
                background.loadWithProgressPlaceholder(modelObject.variationList[2].screenshot)
            }
            fourthVariation.show()
        }else{
            fourthVariation.hide()
        }

        if (modelObject.variationList.size >= 4) {
            fifthVariation.backgroundTintList = ColorStateList.valueOf(modelObject.variationList[3].color)
            fifthVariation.setOnClickListener {
                background.loadWithProgressPlaceholder(modelObject.variationList[3].screenshot)
            }
            fifthVariation.show()
        }else{
            fifthVariation.hide()
        }

        collection.addView(layout)
        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    fun add(item: Template) {
        data.add(item)
        notifyDataSetChanged()
    }

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }
}