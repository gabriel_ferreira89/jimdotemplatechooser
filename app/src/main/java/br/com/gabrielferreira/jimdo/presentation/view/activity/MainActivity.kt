package br.com.gabrielferreira.jimdo.presentation.view.activity

import android.animation.ArgbEvaluator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import br.com.gabrielferreira.jimdo.R
import br.com.gabrielferreira.jimdo.domain.model.Template
import br.com.gabrielferreira.jimdo.presentation.view.MainContract
import br.com.gabrielferreira.jimdo.presentation.view.adapter.TemplateListAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainContract.Presenter, MainContract.View>(),
        MainContract.View {

    private val templateAdapter by lazy {
        TemplateListAdapter(this)
    }

    override fun createPresenter(): MainContract.Presenter {
        getControllerComponent().inject(this)
        return getControllerComponent().mainPresenter()
    }

    companion object {
        fun createIntent(context: Context): Intent = Intent(context, MainActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter?.init()
    }

    override fun setupViewPager() {
        main_view_pager.adapter = templateAdapter
        main_view_pager.clipToPadding = false
        main_view_pager.setPadding(resources.getDimension(R.dimen.template_margin_start).toInt(), 0,
                resources.getDimension(R.dimen.template_margin_end).toInt(), 0)
        main_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                if (position < templateAdapter.count - 1) {
                    setBackgroundColor(ArgbEvaluator().evaluate(positionOffset, templateAdapter.data[position].backgroundColor,
                            templateAdapter.data[position+1].backgroundColor) as Int)
                } else {
                    setBackgroundColor(templateAdapter.data[position].backgroundColor)
                }
            }

            override fun onPageSelected(position: Int) {
            }
        })
    }

    override fun addAdapter(t: Template) {
        templateAdapter.add(t)
    }

    private fun setBackgroundColor(color: Int) {
        main_background.setBackgroundColor(color)
    }
}