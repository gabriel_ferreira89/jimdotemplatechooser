package br.com.gabrielferreira.jimdo.data.model

import com.google.gson.annotations.SerializedName

open class TemplateVariationData(val id: Int? = null,
                                 val name: String? = null,
                                 val active: Boolean? = false,
                                 val icon: String? = null,
                                 @SerializedName("icon-type")
                                 val iconType: String?= null,
                                 val screenshots: Map<String, String> = mapOf())

