package br.com.gabrielferreira.jimdo.domain.repository

import br.com.gabrielferreira.jimdo.domain.model.Template
import io.reactivex.Observable

interface TemplateRepository {
    fun fetchLatestTemplates(): Observable<String>
    fun fetchTemplate(url: String): Observable<Template>
}