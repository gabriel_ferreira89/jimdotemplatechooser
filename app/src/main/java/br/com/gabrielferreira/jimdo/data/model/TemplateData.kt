package br.com.gabrielferreira.jimdo.data.model

class TemplateData(val id: Int? = null,
                   val name: String? = null,
                   val active: Boolean? = false,
                   val screenshots: Map<String, String> = mapOf(),
                   val variations: List<TemplateVariationData> = arrayListOf(),
                   val meta: Map<String, String> = mapOf())

